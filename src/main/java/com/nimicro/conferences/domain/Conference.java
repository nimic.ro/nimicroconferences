package com.nimicro.conferences.domain;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A Conference.
 */
@Document(collection = "conference")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "conference")
public class Conference implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Keyword)
    private String id;

    @Field("name")
    private String name;

    @NotNull
    @Field("participants")
    private Integer participants;

    @NotNull
    @Field("date")
    private ZonedDateTime date;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Conference name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParticipants() {
        return participants;
    }

    public Conference participants(Integer participants) {
        this.participants = participants;
        return this;
    }

    public void setParticipants(Integer participants) {
        this.participants = participants;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public Conference date(ZonedDateTime date) {
        this.date = date;
        return this;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Conference)) {
            return false;
        }
        return id != null && id.equals(((Conference) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Conference{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", participants=" + getParticipants() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
