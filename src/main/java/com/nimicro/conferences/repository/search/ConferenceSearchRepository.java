package com.nimicro.conferences.repository.search;

import com.nimicro.conferences.domain.Conference;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Conference} entity.
 */
public interface ConferenceSearchRepository extends ElasticsearchRepository<Conference, String> {
}
