package com.nimicro.conferences.service;

import com.nimicro.conferences.domain.Conference;
import com.nimicro.conferences.repository.ConferenceRepository;
import com.nimicro.conferences.repository.search.ConferenceSearchRepository;
import com.nimicro.conferences.service.dto.ConferenceDTO;
import com.nimicro.conferences.service.mapper.ConferenceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Conference}.
 */
@Service
public class ConferenceService {

    private final Logger log = LoggerFactory.getLogger(ConferenceService.class);

    private final ConferenceRepository conferenceRepository;

    private final ConferenceMapper conferenceMapper;

    private final ConferenceSearchRepository conferenceSearchRepository;

    public ConferenceService(ConferenceRepository conferenceRepository, ConferenceMapper conferenceMapper, ConferenceSearchRepository conferenceSearchRepository) {
        this.conferenceRepository = conferenceRepository;
        this.conferenceMapper = conferenceMapper;
        this.conferenceSearchRepository = conferenceSearchRepository;
    }

    /**
     * Save a conference.
     *
     * @param conferenceDTO the entity to save.
     * @return the persisted entity.
     */
    public ConferenceDTO save(ConferenceDTO conferenceDTO) {
        log.debug("Request to save Conference : {}", conferenceDTO);
        Conference conference = conferenceMapper.toEntity(conferenceDTO);
        conference = conferenceRepository.save(conference);
        ConferenceDTO result = conferenceMapper.toDto(conference);
        conferenceSearchRepository.save(conference);
        return result;
    }

    /**
     * Get all the conferences.
     *
     * @return the list of entities.
     */
    public List<ConferenceDTO> findAll() {
        log.debug("Request to get all Conferences");
        return conferenceRepository.findAll().stream()
            .map(conferenceMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one conference by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<ConferenceDTO> findOne(String id) {
        log.debug("Request to get Conference : {}", id);
        return conferenceRepository.findById(id)
            .map(conferenceMapper::toDto);
    }

    /**
     * Delete the conference by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        log.debug("Request to delete Conference : {}", id);
        conferenceRepository.deleteById(id);
        conferenceSearchRepository.deleteById(id);
    }

    /**
     * Search for the conference corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    public List<ConferenceDTO> search(String query) {
        log.debug("Request to search Conferences for query {}", query);
        return StreamSupport
            .stream(conferenceSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(conferenceMapper::toDto)
            .collect(Collectors.toList());
    }
}
