package com.nimicro.conferences.service.mapper;

import com.nimicro.conferences.domain.*;
import com.nimicro.conferences.service.dto.ConferenceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Conference} and its DTO {@link ConferenceDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ConferenceMapper extends EntityMapper<ConferenceDTO, Conference> {



    default Conference fromId(String id) {
        if (id == null) {
            return null;
        }
        Conference conference = new Conference();
        conference.setId(id);
        return conference;
    }
}
