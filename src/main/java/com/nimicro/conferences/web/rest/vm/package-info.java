/**
 * View Models used by Spring MVC REST controllers.
 */
package com.nimicro.conferences.web.rest.vm;
